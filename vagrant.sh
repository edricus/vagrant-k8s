#!/bin/bash
colorgreen(){ printf '\033[0;32m'; }
colordefault(){ printf '\033[0m'; }
while getopts "dsrn:h" flag; do
  case $flag in
    h)
  		cat <<- EOF
			Usage: ./vagrant.sh [OPTION]...
			Deploy a Kubernetes lab using Vagrant
			  -n, --number          Deploy n workers
			  -d, --destroy         Destroy all VMs
			  -s, --stop            Stop all VMs
			  -r, --resume          Resume all VMs
			EOF
      ;;
    d|'--destroy')
      for f in `find . -maxdepth 1 -not -path '*/.*' -type d -printf '%P\n' | sed '1d' | sed '/.vagrant/d'`; do
        (cd $f ; vagrant destroy -f) && printf "$f destroyed\n"
        rm -rf $f
      done
      rm -f info
      exit 0
      ;;
    s|'--stop')
      for f in `find . -maxdepth 1 -not -path '*/.*' -type d -printf '%P\n' | sed '1d' | sed '/.vagrant/d'`; do
        (cd $f ; vagrant halt) && printf "$f stopped\n"
      done
      exit 0
      ;;
    r|'--resume')
      for f in `find . -maxdepth 1 -not -path '*/.*' -type d -printf '%P\n' | sed '1d' | sed '/.vagrant/d'`; do
        (cd $f ; vagrant up) && printf "$f started\n"
      done
      exit 0
      ;;
    n|'--number')
    ### Deploy n number of Workers
      if ! [[ $OPTARG =~ ^[0-9]+$ ]]; then
        echo "Veuillez entrer un nombre entier valide."
        exit 1
      fi
      
      ### Compute requirements
      VMS=$(($OPTARG+1))
      REQRAM=$((2048*$VMS))
      RAM=$(free -m | sed '1d;3d' | tr -s ' ' | cut -f7 -d' ')
      CPU=$(lscpu | grep 'CPU(s)' | head -n1 | tr -s ' ' | cut -f2 -d' ')
      if [[ $RAM -lt $REQRAM ]]; then
        echo "Not enough RAM (free: $RAM / required: $REQRAM)"
        exit 1
        else 
          if [[ $CPU -lt $((VMS*2)) ]]; then
            echo "Not enough vCPU (free: $CPU / required: $VMS)"
            exit 1
          fi
      fi
      ### Resume
      printf "Workers: $OPTARG\nMaster: 1\nRAM requise: $((2048*$VMS))Mo\nvCPU requis: $(($VMS))\n"
      read -p "Continuer ? y/N " yn
      if [[ $yn == 'y' || $yn == 'Y' ]]; then
        for ((i=1; i <= OPTARG; i++)); do
          mkdir -p "worker${i}"
        done
        mkdir "master"
      else
        exit 1
      fi
      ### Deploy
      rm -f info
      PORT=3001
      IP=192.168.60.1
      HOST=0
      FOLDERS=$(find . -maxdepth 1 -not -path '*/.*' -type d -printf '%P\n' | sed '1d' | sed '/.vagrant/d' | sort)
      for f in $FOLDERS; do
        colorgreen
        printf "==Configuration de ${f}==\n"
        colordefault
        if [[ ! -f $f/Vagrantfile ]]; then
          rsync Vagrantfile k8s-install.yml $f/
        else
          echo "Vagrantfile found, not overwriting"
          continue
        fi
        sed -i "s/SSH_PORT/$PORT/" $f/Vagrantfile
        sed -i "s/HOSTNAME/$f/" $f/Vagrantfile
        sed -i "s/IP/${IP}${HOST}/" $f/Vagrantfile
        (cd $f ; vagrant up)
        colorgreen
        printf "Pour se connecter à $f : \n  ssh -p $PORT vagrant@127.0.0.1 \n  IP interne : ${IP}${HOST}\n" >> info
        colordefault
        ((HOST++))
        ((PORT++))
      done
      for i in {1..3}; do
        ssh-keygen -f "/home/edricus/.ssh/known_hosts" -R "[127.0.0.1]:300$i"
      done
      scp -o StrictHostKeyChecking=accept-new -P 3001 k8s-control.sh vagrant@127.0.0.1:
      scp -o StrictHostKeyChecking=accept-new -P 3001 cni-install.sh vagrant@127.0.0.1:
      ssh -o StrictHostKeyChecking=accept-new -p 3001 vagrant@127.0.0.1 './k8s-control.sh'
      ssh -o StrictHostKeyChecking=accept-new -p 3001 vagrant@127.0.0.1 './cni-install.sh'
      cat info
      echo "Les informations sont disponibles dans le fichier 'info' à la racine du projet"
      ;;
  esac
done
