#!/bin/bash
sudo kubeadm init \
  --pod-network-cidr 10.0.100.0/24 \
  --apiserver-advertise-address 192.168.60.10 \
  --kubernetes-version 1.28.0
mkdir -p $HOME/.kube  
sudo cp /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
kubectl apply -f https://docs.projectcalico.org/manifests/calico.yaml
while [[ $(kubectl get nodes | grep NotReady) ]]; do
  echo "not ready"
  sleep 30
done
printf '\033[0;33m' # yellow
echo "Commande à éxecuter sur les workers pour les joindre le cluster :"
kubeadm token create --print-join-command | tee -a info
printf '\033[0m' # normal

